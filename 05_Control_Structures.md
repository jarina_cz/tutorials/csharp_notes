  # Control Structures

## If/Else IF/Else Statements

* c-like syntax

```csharp
int j = 10;
if (j == 10)
{
  Console.WriteLine("I get printed");
}
else if (j > 10)
{
  Console.WriteLine("I don't");
}
else
{
  Console.WriteLine("I also don't");
}
```

## Ternary Operators (simplified if/else)

A simple if/else can be written as follows

`<condition> ? <true> : <false>`

```csharp
string isTrue = (true) ? "True" : "False";
```

## While loop

```csharp
int fooWhile = 0;
while (fooWhile < 100)
{
  //Iterated 100 times, fooWhile 0->99
  fooWhile++;
}

// Do While Loop
int fooDoWhile = 0;
do
{
  //Iterated 100 times, fooDoWhile 0->99
  fooDoWhile++;
} while (fooDoWhile < 100);
```

## For loop

for loop structure

`for(<start_statement>; <conditional>; <step>)`

```csharp
for (int fooFor = 0; fooFor < 10; fooFor++)
{
  //Iterated 10 times, fooFor 0->9
}
```

## For Each loop

The foreach loop loops over any object implementing IEnumerable or IEnumerable<T>

All the collection types (Array, List, Dictionary...) in the .Net framework implement one or both of these interfaces.

foreach loop structure

`foreach(<iteratorType> <iteratorName> in <enumerable>)`

```csharp
// (The ToCharArray() could be removed, because a string also implements IEnumerable)
foreach (char character in "Hello World".ToCharArray())
{
  //Iterated over all the characters in the string
}
```

## Switch case

A switch works with the byte, short, char, and int data types.

It also works with enumerated types (discussed in Enum Types), the String class, and a few special classes that wrap primitive types: Character, Byte, Short, and Integer.

```csharp
int month = 3;
string monthString;
switch (month)
{
  case 1:
    monthString = "January";
    break;
  case 2:
    monthString = "February";
    break;
  case 3:
    monthString = "March";
    break;
    // You can assign more than one case to an action
    // But you can't add an action without a break before another case
    // (if you want to do this, you would have to explicitly add a goto case x
  case 6:
  case 7:
  case 8:
    monthString = "Summer time!!";
    break;
  default:
    monthString = "Some other month";
    break;
}
```

