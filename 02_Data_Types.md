# Data Types

## Value Types

The value types directly contain data.

```csharp
// Sbyte - Signed 8-bit integer
// (-128 <= sbyte <= 127)
sbyte fooSbyte = 100;

// Byte - Unsigned 8-bit integer
// (0 <= byte <= 255)
byte fooByte = 100;

// Short - 16-bit integer
// Signed - (-32,768 <= short <= 32,767)
// Unsigned - (0 <= ushort <= 65,535)
short fooShort = 10000;
ushort fooUshort = 10000;

// Integer - 32-bit integer
int fooInt = 1; // (-2,147,483,648 <= int <= 2,147,483,647)
uint fooUint = 1; // (0 <= uint <= 4,294,967,295)

// Long - 64-bit integer
long fooLong = 100000L; // (-9,223,372,036,854,775,808 <= long <= 9,223,372,036,854,775,807)
ulong fooUlong = 100000L; // (0 <= ulong <= 18,446,744,073,709,551,615)
// Numbers default to being int or uint depending on size.
// L is used to denote that this variable value is of type long or ulong

// Double - Double-precision 64-bit IEEE 754 Floating Point
double fooDouble = 123.4; // Precision: 15-16 digits

// Float - Single-precision 32-bit IEEE 754 Floating Point
float fooFloat = 234.5f; // Precision: 7 digits
// f is used to denote that this variable value is of type float

// Decimal - a 128-bits data type, with more precision than other floating-point types,
// suited for financial and monetary calculations
decimal fooDecimal = 150.3m;

// Boolean - true & false
bool fooBoolean = true; // or false

// Char - A single 16-bit Unicode character
char fooChar = 'A';

// Use const or read-only to make a variable immutable
// const values are calculated at compile time
const int HOURS_I_WORK_PER_WEEK = 9001;
```

## Reference types

The reference types do not contain the actual data stored in a variable, but they contain a reference to the variables.

### Object

The **Object Type** is the ultimate base class for all data types in C#. Object is an alias for System.Object class.

```csharp
object obj = 100;
```

### Dynamic

You can store any type of value in the dynamic data type variable. Type 
checking for these types of variables takes place at run-time.

```csharp
dynamic d = 10;
```

### String

The **String Type** allows you to assign any string values to a variable.

It can be declared using the `String` or its alias form `string` which is more commonly used.

```csharp
string fooString = "\"escape\" quotes and add \n (new lines) and \t (tabs)";
// You can access each character of the string with an indexer:
char charFromString = fooString[1]; // => 'e'
// Strings are immutable: you can't do fooString[1] = 'X';

// Compare strings with current culture, ignoring case
string.Compare(fooString, "x", StringComparison.CurrentCultureIgnoreCase);

// Formatting, based on sprintf
string fooFs = string.Format("Check Check, {0} {1}, {0} {1:0.0}", 1, 2);

// You can split a string over two lines with the @ symbol. To escape " use ""
string bazString = @"Here's some stuff
on a new line! ""Wow!"", the masses cried";
```

### Pointer

Pointer type variables store the memory address of another type. 
Pointers in C# have the same capabilities as the pointers in C or C++.

```csharp
char* cptr;
int* iptr;
```

### Custom

User/library defined objects

```csharp
// Dates & Formatting
DateTime fooDate = DateTime.Now;
Console.WriteLine(fooDate.ToString("hh:mm, dd MMM yyyy"));
```



## Converting Data Types and Typecasting

* Convert String to Integer

```csharp
// this will throw an Exception on failure
int.Parse("123");//returns an integer version of "123"

// try parse will default to type default on failure
// in this case: 0
int tryInt;
if (int.TryParse("123", out tryInt)) // Function is boolean
  Console.WriteLine(tryInt);       // 123
```

* Convert Integer to String

```csharp
// Convert class has a number of methods to facilitate conversions
Convert.ToString(123);
// or
tryInt.ToString();
```

