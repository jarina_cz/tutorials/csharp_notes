# .NET

.Net is a platform that helps you to write applications. It is basically a set of Technologies and Libraries.

.NET currently support several languages (e.g. C#, F#, VB, PowerShell)

For the full list of supported languages you can go [here](https://en.wikipedia.org/wiki/List_of_CLI_languages). It might be worth to mention that languages might not be supported equally and some may support more features than others. Most widely used language is C#.

## .NET Versions

* .Net Framework (Windows only)
* .Net Core (multi-platform)
* Xamarin (iOS/Android/Mac OS X)

![1545299344238](assets/1545299344238.png)

Xamarin is however not based on .Net Framework or .Net Core. It is based on **Mono**.

**Mono** is an open-source project lead by Xamarin. It's goal is to be compatible with .NET Framework, but at the same time be cross-platform.

Xamarin was acquired by Microsoft in 2016 and since then, Mono development is sponsored by Microsoft.

### .Net Framework

Is the "full" or "traditional" flavor of .NET that's distributed with 
Windows. Use this when you are building a desktop Windows or UWP app, or
working with older ASP.NET 4.6+.

The last version with new features should be 4.8. As of this version framework will basically switch to maintenance mode and will only receive bug fixes and security updates.

### .NET Core

Is where all the new .Net development is now.

It is a cross-platform .NET that runs on Windows, Mac, and Linux. Use this 
when you want to build console or web apps that can run on any platform,
including inside Docker containers. This does not include UWP/desktop 
apps currently in it's latest version 2+.

However desktop support was announced with the version 3.0 which should come during 2019. Desktop support is Windows-only.



![https://regmedia.co.uk/2018/05/07/dotnetcore3.png?x=442&y=293&crop=1](assets/dotnetcore3.png)

### Xamarin

Is a framework to build native mobile/mac applications using C#.

It allows to some extent to have a shared code between the platforms + native UI + platform specific C# code. It uses Mono under the hood and allows developers the need to use different languages for different platforms (like Java/Kotlin for Android or Objective-C/Swift for iOS/Mac).

![https://visualstudio.microsoft.com/wp-content/uploads/2018/11/TargetAllPlatforms_636x300-grey-600x283.png](assets/TargetAllPlatforms_636x300-grey-600x283.png)

# C-Sharp

C# is a simple, modern, general-purpose, object-oriented programming 
language developed by Anders Hejlsberg and his team at Microsoft during the development of .Net Framework.

* It is a modern, general-purpose programming language

* It is object oriented.

* It is component oriented.
* It is easy to learn.

* It is a structured language.

* It produces efficient programs.

* It can be compiled on a variety of computer platforms.

* It is a part of .Net Framework.

## Basic Syntax

A C# Program consist of the following parts:

* Namespace declaration
* A class
* Class methods
* Class attributes
* A Main method
* Statements and Expressions
* Comments

```csharp
using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");  // Prints hello world to console
          
          	/* Below code waits for user input and I am
          	   a multiline comment */
          	Console.ReadLine();
        }
    }
}
```



## Reserved keywords

There are some reserved keywords that can't be used as a name of class, variable, method, etc.

|           |           | Reserved | Keywords   |                        |                       |         |
| --------- | --------- | :------- | ---------- | ---------------------- | --------------------- | ------- |
| abstract  | as        | base     | bool       | break                  | byte                  | case    |
| catch     | char      | checked  | class      | const                  | continue              | decimal |
| default   | delegate  | do       | double     | else                   | enum                  | event   |
| explicit  | extern    | false    | finally    | fixed                  | float                 | for     |
| foreach   | goto      | if       | implicit   | in                     | in (generic modifier) | int     |
| interface | internal  | is       | lock       | long                   | namespace             | new     |
| null      | object    | operator | out        | out (generic modifier) | override              | params  |
| private   | protected | public   | readonly   | ref                    | return                | sbyte   |
| sealed    | short     | sizeof   | stackalloc | static                 | string                | struct  |
| switch    | this      | throw    | true       | try                    | typeof                | uint    |
| ulong     | unchecked | unsafe   | ushort     | using                  | virtual               | void    |
| volatile  | while     |          |            |                        |                       |         |



|                   | Contextual | Keywords  |            |         |         |                |
| ----------------- | ---------- | --------- | ---------- | ------- | ------- | -------------- |
| add               | alias      | ascending | descending | dynamic | from    | get            |
| global            | group      | into      | join       | let     | orderby | partial (type) |
| partial  (method) | remove     | select    | set        |         |         |                |

