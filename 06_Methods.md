# Methods

A method is a group of statements that together perform a task. Every C#
program has at least one class with a method named Main.

```csharp
public // Visibility
static // Allows for direct call on class without object 
int // Return Type,
MethodSignatures(
  int maxCount, // First variable, expects an int
  int count = 0, // will default the value to 0 if not passed in
  int another = 3,
  params string[] otherParams // captures all other parameters passed to method
)
{ 
  return -1;
}
```

## Method/Function overloading

* Methods can have the same name, as long as the signature is unique

```csharp
using System;

namespace PolymorphismApplication {
   class Printdata {
      void print(int i) {
         Console.WriteLine("Printing int: {0}", i );
      }
      void print(double f) {
         Console.WriteLine("Printing float: {0}" , f);
      }
      void print(string s) {
         Console.WriteLine("Printing string: {0}", s);
      }
      static void Main(string[] args) {
         Printdata p = new Printdata();
         
         // Call print to print integer
         p.print(5);
         
         // Call print to print float
         p.print(500.263);
         
         // Call print to print string
         p.print("Hello C++");
         Console.ReadKey();
      }
   }
}

// Printing int: 5
// Printing float: 500.263
// Printing string: Hello C++
```

